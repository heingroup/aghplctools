"""
A script for extracting signals and report tables from a .D (or series of .D folders). Data will be written in-place
and will consist of:
    - one .csv for each specified signal (this will be the DAD trace for that signal)
    - an .xlsx file containing one sheet for each signal (identical data as csv)
    - an .xlsx file consisting of the report table
"""

import pathlib
import logging
import argparse
import threading
import aghplctools
import time
from tqdm import tqdm
from queue import Queue
from typing import Union, List
from aghplctools.data.sample import HPLCSampleInfo, HPLCSample
from aghplctools.ingestion.text import report_text_to_xlsx

__version__ = '3.4'

logger = logging.getLogger('DI data extraction')


def targets_from_path(specified_path: Union[str, pathlib.Path]) -> List[pathlib.Path]:
    """
    Identify target .D files from the target path

    :param specified_path: pathlike
    :return: list of .D paths
    """
    if isinstance(specified_path, pathlib.Path) is False:
        specified_path = pathlib.Path(specified_path)
    logger.debug(f'searching {specified_path} for .D folders')
    if specified_path.suffix == '.D':
        # if provided with a single .D folder
        out = [specified_path]
    else:
        # search for .D folders under that folder
        out = list(specified_path.glob('**/*.D'))
    if len(out) == 0:
        msg = f'no .D folders were found in the path "{specified_path}"'
        logger.error(msg)
        raise ValueError(msg)
    logger.info(f'identified {len(out)} .D files in the directory "{specified_path}"')
    return out


def convert_data(target: pathlib.Path, overwrite: bool):
    """
    Converts data for a single target

    :param target: target file
    :param overwrite: flag control of data overwrite
    :return:
    """
    logger.debug(f'processing {target.name}')
    try:
        sample_info = HPLCSampleInfo.auto_create(target)
    except IOError as e:
        logger.error(f'no or multiple acaml files were found in the target directory, there must be one. {e}')
        return
    except ValueError as e:
        logger.error(f'the sample context file does not have a valid extension: {e}')
        return
    if all([(target / f'{signal}.csv').is_file() for signal in sample_info.signals]) and not overwrite:
        logger.debug('sample already processed, continuing')
        return
    logger.debug('creating HPLCSample instance')
    sample = HPLCSample.create_from_D_file(target)
    if sample.spectrum is None:
        msg = f'no .UV data was located for sample {target}'
        logger.error(msg)
        raise FileNotFoundError(msg)
    logger.debug('writing signals to csv')
    sample.write_signals_to_csv(overwrite=overwrite)
    # todo optimize this
    # logger.debug('writing signals to xlsx')
    # sample.write_signals_to_xlsx()

    # identify report text file
    txt = list(target.glob(f'**/Report.TXT'))  # todo consider changing this back to an argument
    if len(txt) > 0:
        logger.debug('converting report.txt to xlsx')
        try:
            report_text_to_xlsx(txt[0])
        except (ValueError, KeyError) as e:
            logger.error(e)
        except Exception as e:
            logger.error('unhandled exception in text conversion', exc_info=e)
    logger.debug(f'processing {target.name} complete')


def extract_data_work(q: Queue, overwrite: bool = False):
    """worker function for completing conversion tasks"""
    while True:
        target = q.get()
        try:
            convert_data(target, overwrite)
        except Exception as e:
            logger.exception(f'an error was encountered while converting data for {target}')
        prog.update(1)
        q.task_done()


def perform_data_extraction(target_path,
                            n_workers: int = 1,
                            overwrite_data: bool = False,
                            ):
    """
    Performs DI Data Extraction on the specified path.

    :param target_path: pathlike containing data folders for processing
    :param n_workers: number of workers to use
    :param overwrite_data: whether to overwrite data while processing
    """
    targets = targets_from_path(target_path)

    prog.total = len(targets)

    # create conversion queue and add all targets
    conversion_queue = Queue()
    for target in targets:
        conversion_queue.put(target)

    # create and start workers
    workers = []
    for i in range(n_workers):
        worker = threading.Thread(
            target=extract_data_work,
            args=(conversion_queue, overwrite_data),
            daemon=True
        )
        workers.append(worker)
        logger.debug(f'starting worker #{i + 1}')
        worker.start()

    while conversion_queue.empty() is False:
        prog.refresh()
        time.sleep(1)

    # todo update prog during this last phase
    conversion_queue.join()
    logger.info('data conversion complete')


prog = tqdm(desc='extracting data')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DI data extraction script')

    # target path override (if you're running this as a python script)
    path_override = None

    if not path_override:
        parser.add_argument(
            'path',
            help=(
                'target path for the script. Must be either a path to a .D folder or to a folder which '
                'contains .D folders'
            ),
        )

    parser.add_argument(
        '-ll',
        '--log_level',
        help='logging level to use (see python logging package)',
        type=int,
        dest='log_level',
        default=20,
    )

    # parser.add_argument(  # todo consider reenabling
    #     '-rtn',
    #     '--report_text_name',
    #     help='report text name (usually "Report.TXT")',
    #     dest='report_text_name',
    #     default='Report.TXT',
    # )

    parser.add_argument(
        '--log_file',
        action='store_true',
        help='sets logging to debug mode and logs to file',
        dest='log_file',
    )

    parser.add_argument(
        '--overwrite',
        action='store_true',
        help='overwrite existing csv files',
        dest='overwrite',
    )

    parser.add_argument(
        '--workers',
        type=int,
        help='number of thread workers to use for conversion',
        default=1,  # todo dig into data loading (does not increase efficiency with multiple workers)
    )

    args = parser.parse_args()

    logging.basicConfig(
        level=args.log_level,
        format='%(asctime)s %(name)s %(levelname)s: %(message)s',
    )
    if args.log_file is True:
        logger.setLevel(logging.DEBUG)
        logger.debug('enabling log to file "DI_data_extraction.log"')
        file_handler = logging.FileHandler(
            "DI_data_extraction.log",
        )
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(name)s %(levelname)s: %(message)s'))
        file_handler.setLevel(logging.DEBUG)
        logger.addHandler(file_handler)

    if path_override is None and args.path is None:
        raise ValueError('a target path must be specified')

    logger.info(f'beginning DI data extraction (v{__version__})')
    logger.debug(f'aghplctools version: {aghplctools.__version__}')
    logger.debug(f'path specified by arguments: "{path_override or args.path}"')
    logger.debug(f'log level: {args.log_level}')
    # logger.debug(f'report text name: {args.report_text_name}')

    perform_data_extraction(
        path_override or args.path,
        n_workers=args.workers,
        overwrite_data=args.overwrite,
    )
