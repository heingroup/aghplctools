aghplctools.data package
========================

aghplctools.data.batch module
-----------------------------

.. automodule:: aghplctools.data.batch
   :members:
   :undoc-members:
   :show-inheritance:

aghplctools.data.sample module
------------------------------

.. automodule:: aghplctools.data.sample
   :members:
   :undoc-members:
   :show-inheritance:

aghplctools.data.time\_course module
------------------------------------

.. automodule:: aghplctools.data.time_course
   :members:
   :undoc-members:
   :show-inheritance:
