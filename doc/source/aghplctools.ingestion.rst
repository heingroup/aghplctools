aghplctools.ingestion package
=============================

The ``ingestion`` module contains modules and methods for parsing and ingesting report files produced by Agilent ChemStation.
The exact structure of the exported files may change between installations of ChemStation, but hopefully one of these
tools will work for your purposes.

While the report txt and csv files contain metadata information, a more complete set of metadata may be found in the
``sequence.acam_`` file which may be found in the ``*.D`` directory. We have written ingesters for this metadata and
created data classes around that metadata. You can find these tools in ``data.sample``.

aghplctools.ingestion.csv module
--------------------------------

The ``ingestion.csv`` module contains methods for retrieving peak tables (``pull_hplc_area_from_csv``) and metadata
(``pull_metadata_from_csv``) from csv report files. The structure of the report csv files (numerous files are generated)
appear to follow some arcane method that is difficult to extract context from. These extraction tools have only worked
for us on runs which are not externally referenced. Your mileage may vary.

.. automodule:: aghplctools.ingestion.csv
   :members:
   :undoc-members:
   :show-inheritance:

aghplctools.ingestion.text module
---------------------------------

The `ingestion.text` module contains methods for parsing report text files (named by default ``'Report.TXT'``).
The method you are most likely to use in this module is ``pull_hplc_area_from_txt``. This method pulls an HPLC area table
from a text file when provided with a valid file  path. This method will return a dictionary with the following structure:

Example Output::
    {
        wavelength: {
            retention_time: {
                'Width': float,
                'Area': float,
                'Height': float,
                'Peak': int,
                'Type': str,
                'RetTime': float,
            },
            ...
        },
        ...
    }


``wavelength`` and ``retention_time`` will be floats, and the type of each value is noted in the above dictionary. Regex
matches have been included for each column type we have encountered. If you encounter an error, please create an issue
and provide an example file so that we might expand our matching capabilities.

For convenience, a ``report_text_to_xlsx`` method is included which parses a peak table from a ``Report.TXT`` file and
saves it to an Excel ``xlsx`` file.

.. automodule:: aghplctools.ingestion.text
   :members:
   :undoc-members:
   :show-inheritance:
