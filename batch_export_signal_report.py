from tqdm import tqdm
from hein_utilities.files import Watcher
from aghplctools.data.sample import HPLCSample
from aghplctools.ingestion.text import report_text_to_xlsx

# target folder to batch process
target_folder = 'Z:\\Kea_Comet 2019-12-03 14-57-27'

watch = Watcher(
    path=target_folder,
    watchfor='Report.TXT',
)

print(f'Generating Peak Tables (Excel format) for {len(watch.contents)} files ')
for file in tqdm(watch.contents):
    report_text_to_xlsx(file)

watch.watch_type = 'folder'
watch.watchfor = '*.D'

print(f'Exporting chromatograms to csv and excel for {len(watch.contents)} files')
for data_file in tqdm(watch.contents):
    sample = HPLCSample.create_from_D_file(data_file)
    sample.write_signals_to_xlsx()
    sample.write_signals_to_csv()
