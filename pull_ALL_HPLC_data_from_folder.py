import pathlib
import openpyxl as op
import numpy as np
import logging
from aghplctools.data.batch import pull_hplc_data_from_folder_timepoint
from aghplctools.data.sample import HPLCSampleInfo

__build__ = '3'

# folder to search
targetfolder = ''

# wiggle in retention time
wiggle = 0.1

# ===================================
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

targetfolder = pathlib.Path(targetfolder)

logging.info(f'Extracting data from {targetfolder}')
filenames, targets = pull_hplc_data_from_folder_timepoint(  # pull all the data
    folder=targetfolder,
    wiggle=wiggle,
)

if len(targets) == 0:
    raise FileNotFoundError(f'It looks like there are no Report.txt files in the target folder ({targetfolder}).'
                            f'Please verify the folder path and check that there are Report.txt files present in '
                            f'the data folders. ')

# create excel file object
xlfile = op.workbook.Workbook()
filenames = [pathlib.Path(filename) for filename in filenames]

try:
    logger.debug('attempting to load datetime from sample metadata')
    sample_times = [
        HPLCSampleInfo.auto_create(file_path.parent).datetimestamp
        for file_path in filenames
    ]
except Exception as e:
    logger.error(f'could not determine sample times from metadata: {e}')
    sample_times = None

logger.info('writing data to workbook')
for wavelength in targets:  # for each wavelength
    logger.debug(f'writing {wavelength}')
    sheet = xlfile.create_sheet(f'{wavelength} nm')
    # sort targets for this wavelength
    wavelength_targets = [targets[wavelength][rt] for rt in sorted(targets[wavelength])]
    # add header row
    header = [
        'Filename',
        *[str(target.retention_time) for target in wavelength_targets]
    ]
    if sample_times is not None:
        header.insert(1, 'Timestamp')
    sheet.append(header)

    # append data (transpose)
    areas = np.asarray([target.areas for target in wavelength_targets])
    for ind, row in enumerate(areas.T):
        row_values = [
            filenames[ind].parts[-2],
            *row,
        ]
        if sample_times is not None:
            row_values.insert(1, sample_times[ind])
        sheet.append(row_values)

logger.debug(f'removing default sheet')
del xlfile['Sheet']
target_file = f'{targetfolder / f"{targetfolder.parts[-1]} extracted data.xlsx"}'
logger.info(f'saving data to {target_file}')
xlfile.save(target_file)  # save file
logger.info('fin.')
