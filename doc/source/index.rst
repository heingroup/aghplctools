Welcome to aghplctools's documentation!
=======================================

The `aghplctools` package is created by the Hein Group at the University of
British Columbia (UBC) which provides data types and tools for interacting
with Agilent ChemStation.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   aghplctools


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
